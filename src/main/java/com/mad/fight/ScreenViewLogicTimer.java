package com.mad.fight;

import java.util.ArrayList;
import java.util.List;

/**
 * This class updates updatables provided by the ScreenViewSelector
 * specified in the constructor.
 */
public class ScreenViewLogicTimer extends AbstractLogicTimer {
    private final ScreenViewSelector screenViewSelector;

    public ScreenViewLogicTimer(ScreenViewSelector screenViewSelector) {
        this.screenViewSelector = screenViewSelector;
    }

    @Override
    protected void update() {
        List<Updatable> updatables = new ArrayList<>(screenViewSelector.getUpdatables());
        for (Updatable u : updatables) {
            u.update(getUpdateCount());
        }
    }
}
