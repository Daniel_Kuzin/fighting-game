package com.mad.fight.components;

import com.mad.fight.*;
import javafx.event.EventHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

import java.awt.*;
import java.util.function.Consumer;

public class Slider implements Drawable, MouseComponent, MouseMoveListener, MouseClickListener {
    private static final Image TIC_ENABLED = new Image("png/settings/tic0.png");
    private static final Image TIC_MOUSEOVER = new Image("png/settings/tic2.png");
    private static final Image TIC_DISABLED = new Image("png/settings/tic1.png");

    private static final Image LEFT_BTN = new Image("png/settings/leftbtn.png");
    private static final Image LEFT_BTN_MO = new Image("png/settings/leftbtnc.png");
    private static final Image RIGHT_BTN = new Image("png/settings/rghtbtn.png");
    private static final Image RIGHT_BTN_MO = new Image("png/settings/rghtbtnc.png");

    private static final int WIDTH = 4 * 10;

    private final ImageButton downButton;
    private final ImageButton upButton;

    private final DrawingContextProvider drawingContextProvider;
    private Position position;
    private int value = 10;

    private int mouseOverValue;
    private Consumer<Integer> onValueChanged = value -> {
    };

    public Slider(DrawingContextProvider drawingContextProvider, Position position) {
        this.drawingContextProvider = drawingContextProvider;
        this.position = position;

        DrawingContext dc = drawingContextProvider.getDrawingContext();

        Position downButtonPos = new Position(
                Anchor.TOP_LEFT,
                false,
                false,
                position.getX(dc),
                position.getY(dc));

        this.downButton = new ImageButton(LEFT_BTN, downButtonPos, drawingContextProvider)
                .withMouseoverImage(LEFT_BTN_MO)
                .withClickAction(() -> {
                    if (this.value > 0) {
                        value--;
                        this.onValueChanged.accept(value);
                    }
                });

        Rectangle bounds = getBounds();

        Position upButtonPos = new Position(
                Anchor.TOP_RIGHT,
                false,
                false,
                bounds.x + bounds.width,
                position.getY(dc));

        this.upButton = new ImageButton(RIGHT_BTN, upButtonPos, drawingContextProvider)
                .withMouseoverImage(RIGHT_BTN_MO)
                .withClickAction(() -> {
                    if (this.value < 10) {
                        value++;
                        this.onValueChanged.accept(value);
                    }
                });
    }

    public Slider withOnValueChanged(Consumer<Integer> onValueChanged) {
        this.onValueChanged = onValueChanged;
        return this;
    }

    public Slider withValue(int value) {
        this.value = value;
        return this;
    }


    private Rectangle getBounds() {
        DrawingContext dc = drawingContextProvider.getDrawingContext();
        return new Rectangle(position.getX(dc), position.getY(dc), 6 * 2 + 4 * 10, 7);
    }

    private Rectangle getScaledSliderBounds() {
        DrawingContext dc = drawingContextProvider.getDrawingContext();

        int sf = dc.getScalingFactor();

        Rectangle bounds = getBounds();
        Rectangle sliderBounds = new Rectangle(bounds.x + 6, bounds.y, bounds.width - 12, bounds.height);
        return new Rectangle(sliderBounds.x * sf, sliderBounds.y * sf, sliderBounds.width * sf, sliderBounds.height * sf);
    }

    @Override
    public EventHandler<MouseEvent> getMouseClickListener() {
        return event -> {
            upButton.getMouseClickListener().handle(event);
            downButton.getMouseClickListener().handle(event);

            int sf = drawingContextProvider.getDrawingContext().getScalingFactor();
            Rectangle scaledBounds = getScaledSliderBounds();

            if (scaledBounds.contains(event.getX(), event.getY())) {
                value = (int) ((event.getX() - scaledBounds.getX()) / (4 * sf)) + 1;
                this.onValueChanged.accept(value);
                SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.click);
            }
        };
    }

    @Override
    public EventHandler<MouseEvent> getMouseMoveListener() {
        return event -> {
            upButton.getMouseMoveListener().handle(event);
            downButton.getMouseMoveListener().handle(event);

            int sf = drawingContextProvider.getDrawingContext().getScalingFactor();
            Rectangle scaledBounds = getScaledSliderBounds();

            if (scaledBounds.contains(event.getX(), event.getY())) {
                mouseOverValue = (int) ((event.getX() - scaledBounds.getX()) / (4 * sf)) + 1;
            } else {
                mouseOverValue = 0;
            }
        };
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        // 6 is the width of the left button
        int xOffset = 6;

        for (int i = 1; i <= 10; i++) {
            if (i <= mouseOverValue) {
                PixelArtRenderer.render(TIC_MOUSEOVER, new Point(position.getX(dc) + xOffset, position.getY(dc)), dc, gc);
            } else if (i <= value) {
                PixelArtRenderer.render(TIC_ENABLED, new Point(position.getX(dc) + xOffset, position.getY(dc)), dc, gc);
            } else {
                PixelArtRenderer.render(TIC_DISABLED, new Point(position.getX(dc) + xOffset, position.getY(dc)), dc, gc);
            }

            xOffset += 4;
        }

        upButton.draw(gc, updateCount, dc);
        downButton.draw(gc, updateCount, dc);
    }
}
