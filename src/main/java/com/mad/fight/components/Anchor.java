package com.mad.fight.components;

import java.awt.*;

public enum Anchor {
    BOTTOM_RIGHT,
    BOTTOM_LEFT,
    TOP_LEFT,
    TOP_RIGHT,
    TOP_CENTER;

    public static Point anchorOffset(Anchor anchor, Dimension dimension) {
        switch (anchor) {
            case TOP_LEFT:
                return new Point(0, 0);
            case TOP_RIGHT:
                return new Point(-dimension.width, 0);
            case BOTTOM_LEFT:
                return new Point(0, -dimension.height);
            case BOTTOM_RIGHT:
                return new Point(-dimension.width, -dimension.height);
            case TOP_CENTER:
                return new Point(-dimension.width / 2, 0);
            default:
                throw new IllegalStateException();
        }
    }
}
