package com.mad.fight.components;

import com.mad.fight.DrawingContextProvider;
import com.mad.fight.SoundEffectsManager;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.awt.*;

public class TextButton extends Text implements MouseClickListener, MouseMoveListener, MouseComponent {
    private final DrawingContextProvider drawingContextProvider;

    private Runnable action = () -> {};
    private boolean hoverActive;

    private EventHandler<MouseEvent> mouseClickListener = event -> {
        Rectangle bounds = getScaledBounds();

        if (event.getX() > bounds.x &&
                event.getX() < bounds.x + bounds.width &&
                event.getY() > bounds.y &&
                event.getY() < bounds.y + bounds.height) {
            SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.click);
            this.action.run();
        }
    };

    private EventHandler<MouseEvent> mouseMoveListener = event -> {
        Rectangle bounds = getScaledBounds();

        if (event.getX() > bounds.x &&
                event.getX() < bounds.x + bounds.width &&
                event.getY() > bounds.y &&
                event.getY() < bounds.y + bounds.height) {
            if (!hoverActive) {
                SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.mouseover);
            }

            hoverActive = true;
        } else {
            hoverActive = false;
        }
    };

    public TextButton(String text, Position position, DrawingContextProvider drawingContextProvider) {
        super(text, position);
        this.drawingContextProvider = drawingContextProvider;
    }

    public TextButton withAction(Runnable action) {
        this.action = action;
        return this;
    }

    private Rectangle getScaledBounds() {
        Rectangle bounds = getBounds(drawingContextProvider.getDrawingContext());
        int sf =  drawingContextProvider.getDrawingContext().getScalingFactor();
        return new Rectangle(bounds.x * sf, bounds.y * sf, bounds.width * sf, bounds.height * sf);
    }

    @Override
    public EventHandler<MouseEvent> getMouseClickListener() {
        return mouseClickListener;
    }

    @Override
    public EventHandler<MouseEvent> getMouseMoveListener() {
        return mouseMoveListener;
    }

    @Override
    protected Color getColor() {
        if (hoverActive) {
            return Color.web("#ffffb3");
        } else {
            return Color.web("#ffffff");
        }
    }
}
