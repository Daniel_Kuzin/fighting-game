package com.mad.fight;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * This class contains basic logic that can be used to serializable store data,
 * and notify listening classes about changes to the data
 * @param <T> The type of data to be stored
 */
public abstract class FileBasedStorageHandler<T> {
    /**
     * This set contains all the Consumers (void functions with one parameter) that
     * are listening for changes
     */
    private Set<Consumer<T>> changedListeners = new HashSet<>();

    /**
     * This function calls accept (execute the function) for all the listening consumers
     * @param obj The new data that is to be broadcasted
     */
    private void notifyOfChange(T obj) {
        for (Consumer<T> consumer : changedListeners) {
            consumer.accept(obj);
        }
    }

    /**
     * Adds a Consumer that will be notified of data updates
     * @param consumer The consumer to be notified
     */
    public void addChangedHandler(Consumer<T> consumer) {
        changedListeners.add(consumer);
    }

    /**
     * Returns the platform appropriate data storage directory
     */
    protected Path getAppDataPath() {
        String os = (System.getProperty("os.name")).toUpperCase();

        Path appDataPath;

        if (os.contains("WIN")) {
            appDataPath = Paths.get(System.getenv("AppData"));
        } else if (os.contains("MAC")) {
            appDataPath = Paths.get(System.getProperty("user.home"), "Library", "Application Support");
        } else {
            appDataPath = Paths.get(System.getProperty("user.home"));
        }

        Path path = appDataPath.resolve("FightingGame");

        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                throw new RuntimeException("Failed to create game save directory ", e);
            }
        }

        return path;
    }

    /**
     * Deserializes and returns that data in the specified file.
     * @param path The path of the file to read
     * @return The deserialized data
     */
    protected T load(Path path) {
        try (FileInputStream fos = new FileInputStream(path.toFile())) {
            try (ObjectInputStream oos = new ObjectInputStream(fos)) {
                //noinspection unchecked
                return (T) oos.readObject();
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException("Failed to load settings", e);
        }
    }

    /**
     * Creates or updates the file at the specified path, and broadcasts the new
     * data to all consumers.
     * @param obj The new data to store
     * @param path The path of the file to store the data in
     */
    protected void save(T obj, Path path) {
        try (FileOutputStream fos = new FileOutputStream(path.toFile())) {
            try (ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(obj);
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to load settings", e);
        }

        notifyOfChange(obj);
    }
}
