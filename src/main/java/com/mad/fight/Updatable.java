package com.mad.fight;

public interface Updatable {
    void update(long updateCount);
}
