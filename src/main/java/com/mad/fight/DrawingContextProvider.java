package com.mad.fight;

public class DrawingContextProvider {
    private static DrawingContext drawingContext = new DrawingContext(240, 135, SettingsManager.INSTANCE.getSettings().getScalingFactor());

    public static void initialize(int scalingFactor) {
        drawingContext = new DrawingContext(240, 135, scalingFactor);
    }

    public DrawingContext getDrawingContext() {
        return drawingContext;
    }
}
