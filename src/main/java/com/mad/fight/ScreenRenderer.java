package com.mad.fight;

import com.mad.fight.*;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;

/**
 * This class draws drawables provided by the ScreenViewSelector
 * specified in the constructor.
 */
public class ScreenRenderer extends AnimationTimer {
    private final GraphicsContext graphicsContext;
    private final ScreenViewSelector screenViewSelector;
    private final DrawingContextProvider drawingContextProvider;

    public ScreenRenderer(GraphicsContext graphicsContext, ScreenViewSelector screenViewSelector, DrawingContextProvider drawingContextProvider) {
        this.graphicsContext = graphicsContext;
        this.screenViewSelector = screenViewSelector;
        this.drawingContextProvider = drawingContextProvider;
    }

    @Override
    public void handle(long now) {
        DrawingContext dc = drawingContextProvider.getDrawingContext();

        for (Drawable d : screenViewSelector.getDrawables()) {
            d.draw(graphicsContext, 0, dc);
        }
    }
}
