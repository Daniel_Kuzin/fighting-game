package com.mad.fight.menus.lobby;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import com.mad.fight.DrawingContextProvider;
import com.mad.fight.GameLifecycleManager;
import com.mad.fight.components.*;
import com.mad.fight.fight.Animation;
import com.mad.fight.fight.AnimationHandler;
import com.mad.fight.menus.MenuScreenView;
import javafx.scene.image.Image;

import java.awt.*;

import static com.mad.fight.fight.Animation.Frame.f;

public class LobbyView extends MenuScreenView {
    private static final Image background = new Image("png/lobby/background.png");
    private static final Image foreground = new Image("png/mainmenu/foreground2.png");
    private static final Image select = new Image("png/lobby/select.png");
    private static final Image shadow = new Image("png/character/shadow1.png");
    private static final Image cursor = new Image("png/lobby/cursor.png");
    private final DrawingContextProvider drawingContextProvider;

    private static final Animation BORIS_IDLE = new Animation(
            true,
            f(new Image("png/character/boris_idle_1.png"), 10, 0, 0),
            f(new Image("png/character/boris_idle_2.png"), 10, 0, 0),
            f(new Image("png/character/boris_idle_3.png"), 10, 0, 0)
    );

    private GameLifecycleManager.PlayerNumber playerNumber;

    private final AnimationHandler playerYouAnimationHandler = new AnimationHandler(BORIS_IDLE);
    private final AnimationHandler playerOpponentAnimationHandler = new AnimationHandler(BORIS_IDLE);

    private TextButton readyButton;
    private TextButton unreadyButton;
    private TextButton quitButton;
    private Text playerYou;
    private Text playerOpponent;
    private Text playerReadyMarker;
    private Text opponentReadyMarker;
    private ImageAsset playerYouShadow;
    private ImageAsset playerOpponentShadow;

    private Drawable playerYouCharacter;
    private int playerYouX;
    private Drawable playerOpponentCharacter;
    private int playerOpponentX;


    public LobbyView(LobbyController lobbyController, DrawingContextProvider drawingContextProvider) {
        this.drawingContextProvider = drawingContextProvider;
        DrawingContext dc = drawingContextProvider.getDrawingContext();

        playerYouCharacter = (gc, updateCount, dc1) -> playerYouAnimationHandler.draw(dc1, gc, new Point(playerYouX - 20, 40), GameLifecycleManager.PlayerNumber.TWO.equals(playerNumber));
        playerOpponentCharacter = (gc, updateCount, dc1) -> playerYouAnimationHandler.draw(dc1, gc, new Point(playerOpponentX - 20, 40), GameLifecycleManager.PlayerNumber.ONE.equals(playerNumber));

        addUpdatable(updateCount -> {
            playerYouAnimationHandler.advanceLogic();
            playerOpponentAnimationHandler.advanceLogic();
        });

        playerYouShadow = new ImageAsset(shadow, new Position(Anchor.TOP_CENTER, false, false, 0, 200));

        addDrawable(new ImageAsset(background, new Position(Anchor.TOP_LEFT, false, false, 0, 0)));

        addDrawable(new ImageAsset(select, new Position(Anchor.TOP_CENTER, true, false, 0, 70)));

        addDrawable(new ImageAsset(cursor, new Position(Anchor.TOP_CENTER, false, false, 111, 70)));


        addDrawable(new Text("player 1", new Position(Anchor.TOP_CENTER, false, false, 35, 30)));

        //TODO:Make this button disconnect
        addMouseComponent(new TextButton("quit", new Position(Anchor.TOP_CENTER, true, false, 0, 110), drawingContextProvider).withAction(lobbyController::disconnect));

        addDrawable(new Text("player 2", new Position(Anchor.TOP_CENTER, false, false, dc.getWidth() - 35, 30)));

        playerYou = new Text("you", new Position(Anchor.TOP_CENTER, false, false, 35, 120));
        addDrawable(playerYou);

        playerOpponent = new Text("opponent", new Position(Anchor.TOP_CENTER, false, false, 35, 120));
        addDrawable(playerOpponent);


        createPlayerOneView();

        readyButton = new TextButton("ready", new Position(Anchor.TOP_CENTER, true, false, 0, 40), drawingContextProvider)
                .withAction(lobbyController::ready);

        unreadyButton = new TextButton("unready", new Position(Anchor.TOP_CENTER, true, false, 0, 40), drawingContextProvider)
                .withAction(lobbyController::unready);

        addDrawable(new ImageAsset(foreground, new Position(Anchor.TOP_LEFT, false, false, -6, 80)));
    }

    private void createPlayerOneView() {
        playerNumber = GameLifecycleManager.PlayerNumber.ONE;
        playerYou.setPosition(new Position(Anchor.TOP_CENTER, false, false, 35, 20));
        playerOpponent.setPosition(new Position(Anchor.TOP_CENTER, false, false, drawingContextProvider.getDrawingContext().getWidth() - 35, 20));

        playerYouX = 35;
        playerYouShadow.setPosition(new Position(Anchor.TOP_CENTER, false, false, 36, 94));

        getDrawables().add(getDrawables().size() - 2, playerYouShadow);
        getDrawables().add(getDrawables().size() - 2, playerYouCharacter);

    }

    private void createPlayerTwoView() {
        playerNumber = GameLifecycleManager.PlayerNumber.TWO;
        playerYou.setPosition(new Position(Anchor.TOP_CENTER, false, false, drawingContextProvider.getDrawingContext().getWidth() - 35, 20));
        playerOpponent.setPosition(new Position(Anchor.TOP_CENTER, false, false, 35, 20));

        playerYouX = drawingContextProvider.getDrawingContext().getWidth() - 35;
        playerYouShadow.setPosition(new Position(Anchor.TOP_CENTER, false, false, drawingContextProvider.getDrawingContext().getWidth() - 36, 94));

        getDrawables().add(getDrawables().size() - 2, playerYouShadow);
        getDrawables().add(getDrawables().size() - 2, playerYouCharacter);
    }

    void showPlayerReadyMarker() {
        int xPos;

        if (GameLifecycleManager.PlayerNumber.ONE.equals(playerNumber)) {
            xPos = 35;
        } else if (GameLifecycleManager.PlayerNumber.TWO.equals(playerNumber)) {
            xPos = drawingContextProvider.getDrawingContext().getWidth() - 35;
        } else {
            throw new IllegalStateException();
        }

        playerReadyMarker = new Text("ready", new Position(Anchor.TOP_CENTER, false, false, xPos, 10));
        addDrawable(playerReadyMarker);
    }

    void hidePlayerReadyMarker() {
        removeDrawable(playerReadyMarker);
    }

    void showOpponentReadyMarker() {
        int xPos;

        if (GameLifecycleManager.PlayerNumber.ONE.equals(playerNumber)) {
            xPos = drawingContextProvider.getDrawingContext().getWidth() - 35;
        } else if (GameLifecycleManager.PlayerNumber.TWO.equals(playerNumber)) {
            xPos = 35;
        } else {
            throw new IllegalStateException();
        }

        playerOpponentX = xPos;
        playerOpponentShadow = new ImageAsset(shadow, new Position(Anchor.TOP_CENTER, false, false, xPos, 94));
        opponentReadyMarker = new Text("ready", new Position(Anchor.TOP_CENTER, false, false, xPos, 10));

        getDrawables().add(getDrawables().size() - 2, playerOpponentShadow);
        getDrawables().add(getDrawables().size() - 2, playerOpponentCharacter);
        addDrawable(opponentReadyMarker);
    }

    void hideOpponentReadyMarker() {
        removeDrawable(opponentReadyMarker);
        removeDrawable(playerOpponentShadow);
        removeDrawable(playerOpponentCharacter);
    }

    void showReadyButton() {
        addMouseComponent(readyButton);
    }

    void showUnreadyButton() {
        addMouseComponent(unreadyButton);
    }

    void hideReadyButton() {
        removeMouseComponent(readyButton);
    }

    void hideUnreadyButton() {
        removeMouseComponent(unreadyButton);
        removeDrawable(playerReadyMarker);
    }

    public void layoutForPlayerNumber(GameLifecycleManager.PlayerNumber playerNumber) {
        if (GameLifecycleManager.PlayerNumber.ONE.equals(playerNumber)) {
            createPlayerOneView();
        } else if (GameLifecycleManager.PlayerNumber.TWO.equals(playerNumber)) {
            createPlayerTwoView();
        } else {
            throw new IllegalStateException();
        }
    }
}
