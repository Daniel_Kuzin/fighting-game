package com.mad.fight.menus;

import com.mad.fight.*;
import com.mad.fight.components.Anchor;
import com.mad.fight.components.Position;
import com.mad.fight.components.Text;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;

import java.util.ArrayList;
import java.util.List;

public class IntroScreen implements ScreenView {
    private final Text m = new Text("m", new Position(Anchor.TOP_CENTER, false, false, 105, 50));
    private final Text a = new Text("a", new Position(Anchor.TOP_CENTER, false, false, 120, 50));
    private final Text d = new Text("d", new Position(Anchor.TOP_CENTER, false, false, 135, 50));
    private final Text studios = new Text("studios", new Position(Anchor.TOP_CENTER, true, false, 0, 70));
    private final Image testImage = new Image("/png/map/healthbar.png");

    private final List<Drawable> drawables = new ArrayList<>();
    private final List<Updatable> updatables = new ArrayList<>();
    private final DisplayTimer displayTimer;

    public IntroScreen(ScreenViewFactory screenViewFactory, ScreenViewSelector screenViewSelector) {
        drawables.add((gc, updateCount, dc) -> {
            gc.setFill(Paint.valueOf("#fff"));
            gc.fillRect(0, 0, dc.getScreenWidth(), dc.getScreenHeight());
        });

        drawables.add(m);
        drawables.add(a);
        drawables.add(d);
        drawables.add(studios);

        int displayTime = 60 * 5;

        if(Program.DEBUG_OPTIONS.contains("skipIntro")) {
            displayTime = 1;
        }

        displayTimer = new DisplayTimer(displayTime, () -> screenViewSelector.setCurrentScreen(screenViewFactory.createMainMenuView()));
        updatables.add(displayTimer);
    }

    @Override
    public void onSelected() {
        SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.intro);
    }

    @Override
    public void setHandlers(Canvas canvas) {

    }

    @Override
    public void clearHandlers(Canvas canvas) {

    }

    @Override
    public List<Drawable> getDrawables() {
        return drawables;
    }

    @Override
    public List<Updatable> getUpdatables() {
        return updatables;
    }

    private static class DisplayTimer implements Updatable {
        private final Runnable timerExpiredAction;
        private int timer;

        DisplayTimer(int time, Runnable timerExpiredAction) {
            this.timer = time;
            this.timerExpiredAction = timerExpiredAction;
        }

        @Override
        public void update(long updateCount) {
            if (timer > 0) {
                timer--;

                if (timer == 0) {
                    timerExpiredAction.run();
                }
            }
        }
    }
}
