package com.mad.fight.menus;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;

public class BlackBackground implements Drawable {
    private final Paint COLOR = Paint.valueOf("#F0F");

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        gc.setFill(COLOR);
        gc.fillRect(0, 0, dc.getScreenWidth(), dc.getScreenHeight());
    }
}