package com.mad.fight.menus.pause;

import com.mad.fight.MusicManager;
import com.mad.fight.ScreenViewFactory;
import com.mad.fight.ScreenViewSelector;
import com.mad.fight.SoundEffectsManager;
import com.mad.fight.menus.AbstractMenuController;
import sun.applet.Main;

public class PauseController extends AbstractMenuController {

    public PauseController(ScreenViewSelector screenViewSelector, ScreenViewFactory screenViewFactory) {
        super(screenViewSelector, screenViewFactory);
    }

    void resumeGame() {
        screenViewSelector.removeOverlayScreen();
    }

    void quit() {
        screenViewSelector.removeOverlayScreen();
        screenViewSelector.setCurrentScreen(screenViewFactory.createMainMenuView());
    }
}
