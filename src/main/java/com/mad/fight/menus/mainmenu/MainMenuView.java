package com.mad.fight.menus.mainmenu;

import com.mad.fight.DrawingContextProvider;
import com.mad.fight.components.Anchor;
import com.mad.fight.components.ImageAsset;
import com.mad.fight.components.Position;
import com.mad.fight.components.TextButton;
import com.mad.fight.menus.BlackBackground;
import com.mad.fight.menus.MenuScreenView;
import javafx.scene.image.Image;

@SuppressWarnings("FieldCanBeLocal")
public class MainMenuView extends MenuScreenView {
    private static final Image background = new Image("png/mainmenu/background.png");
    private static final Image logo = new Image("png/mainmenu/logoscaled.png");
    private static final Image foreground = new Image("png/mainmenu/foreground.png");

    public MainMenuView(MainMenuController mainMenuController, DrawingContextProvider drawingContextProvider) {
        addDrawable(new ImageAsset(background,new Position(Anchor.TOP_LEFT,false,false,0,0)));
        addDrawable(new ImageAsset(logo,new Position(Anchor.TOP_CENTER,true,false,0,0)));
        addDrawable(new ImageAsset(foreground,new Position(Anchor.TOP_LEFT,false,false,0,0)));

        addMouseComponent(new TextButton("start game", Position.centeredX(70), drawingContextProvider).withAction(mainMenuController::startGame));

        addMouseComponent(new TextButton("settings", Position.centeredX(90), drawingContextProvider).withAction(mainMenuController::goToSettings));

        addMouseComponent(new TextButton("quit", Position.centeredX(110), drawingContextProvider).withAction(mainMenuController::quit));
    }
}
