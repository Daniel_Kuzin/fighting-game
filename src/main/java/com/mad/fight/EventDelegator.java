package com.mad.fight;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

public abstract class EventDelegator<T, E> {
    private final Set<T> eventHandlers = Collections.synchronizedSet(new HashSet<>());

    public void clear() {
        eventHandlers.clear();
    }

    public void addHandler(T handler) {
        eventHandlers.add(handler);
    }

    public void removeHandler(T handler) {
        eventHandlers.remove(handler);
    }

    public void handle(E event) {
        List<T> eventHandlersC = new ArrayList<>(eventHandlers);
        eventHandlersC.forEach(eh -> delegateEvent(eh, event));
    }

    protected abstract void delegateEvent(T eventHandler, E event);
}
