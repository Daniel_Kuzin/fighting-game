package com.mad.fight;

import com.sun.javafx.image.IntToBytePixelConverter;
import com.sun.javafx.image.impl.IntArgb;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class PixelArtRenderer {
    private static final Map<CacheKey, WritableImage> scaleBuffer = new HashMap<>();
    private static final IntToBytePixelConverter conv = IntArgb.ToByteBgraPreConverter();

    static {
        SettingsManager.INSTANCE.addChangedHandler(ignored -> scaleBuffer.clear());
    }

    @Data
    private static class CacheKey {
        private final Image image;
        private final boolean mirror;
    }

    public static void render(Image image, Point position, DrawingContext dc, GraphicsContext gc) {
        render(image, position, false, new Crop(), dc, gc);
    }

    public static void render(Image image, Point position, boolean mirror, Crop crop, DrawingContext dc, GraphicsContext gc) {

        PixelReader pixelReader = image.getPixelReader();
        int sf = dc.getScalingFactor();
        int w = image.widthProperty().intValue(), h = image.heightProperty().intValue();

        WritableImage scaledImage = scaleBuffer.get(new CacheKey(image, mirror));

        if (scaledImage == null) {
            int[] intBuffer = new int[w * sf * h * sf];

            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    int argb = pixelReader.getArgb(x, y);

                    int x1 = x * sf, y1 = y * sf;

                    for (int c1 = 0; c1 < sf; c1++) {
                        for (int c2 = 0; c2 < sf; c2++) {
                            intBuffer[(x1 + c1) + (w * sf * (y1 + c2))] = argb;
                        }
                    }
                }
            }


            byte[] buffer = new byte[intBuffer.length * 4];
            conv.convert(intBuffer, 0, w, buffer, 0, w * 4, w, buffer.length / (w * 4));

            scaledImage = new WritableImage(w * sf, h * sf);
            scaledImage.getPixelWriter().setPixels(
                    0,
                    0,
                    w * sf,
                    h * sf,
                    PixelFormat.getByteBgraPreInstance(),
                    buffer,
                    0,
                    w * sf * 4);

            scaleBuffer.put(new CacheKey(image, false), scaledImage);
        }

        int width = scaledImage.widthProperty().intValue();
        int height = scaledImage.heightProperty().intValue();

        if (mirror) {
            WritableImage mirroredImage = scaleBuffer.get(new CacheKey(image, true));

            if (mirroredImage == null) {
                mirroredImage = new WritableImage(width, height);
                PixelWriter pw = mirroredImage.getPixelWriter();
                PixelReader pr = scaledImage.getPixelReader();

                byte[] rowBuffer = new byte[width * 4];

                for (int i = 0; i < height; i++) {
                    pr.getPixels(0, i, width, 1, PixelFormat.getByteBgraInstance(), rowBuffer, 0, 0);
                    reverseArray(rowBuffer);
                    pw.setPixels(0, i, width, 1, PixelFormat.getByteBgraInstance(), rowBuffer, 0, 0);
                }

                scaleBuffer.put(new CacheKey(image, true), mirroredImage);
            }

            scaledImage = mirroredImage;
        }

        if (crop.left > 0 || crop.right > 0 || crop.top > 0 || crop.bottom > 0) {
            PixelWriter pixelWriter = gc.getPixelWriter();

            //pixelWriter.setPixels((position.x + crop.left) * sf, (position.y + crop.top) * sf, (width - crop.right) * sf - 1, (height - crop.bottom) * sf - 1, scaledImage.getPixelReader(), crop.left * sf, crop.top * sf);
            pixelWriter.setPixels((position.x + crop.left) * sf,
                    (position.y + crop.top) * sf,
                    (width - (crop.right * sf) - (crop.left) * sf),
                    height - (crop.top * sf) - (crop.bottom * sf),
                    scaledImage.getPixelReader(),
                    0,
                    0);

        } else {
            gc.drawImage(scaledImage, position.getX() * sf, position.getY() * sf);
        }
    }

    private static void reverseArray(byte[] arr) {
        int halfLength = arr.length / 2;

        for (int i = 0; i < halfLength; i += 4) {
            swap(arr, i, arr.length - i - 4);
            swap(arr, i + 1, arr.length - i - 3);
            swap(arr, i + 2, arr.length - i - 2);
            swap(arr, i + 3, arr.length - i - 1);
        }
    }

    private static void swap(byte[] arr, int p1, int p2) {
        byte temp = arr[p1];
        arr[p1] = arr[p2];
        arr[p2] = temp;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Crop {
        private int left;
        private int bottom;
        private int right;
        private int top;
    }
}