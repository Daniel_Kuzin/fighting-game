package com.mad.fight;

import com.mad.fight.net.*;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.jspace.RemoteSpace;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

@Log4j2
public class GameLifecycleManager {
    private static final String BASE_URI;

    static {
        Properties properties = new Properties();

        try {
            properties.load(GameLifecycleManager.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        BASE_URI = "tcp://" + properties.getOrDefault("host", "localhost:8080") + "/";
        log.info("Using host " + BASE_URI);
    }

    private static final String INCOMING_MATCHMAKING_CHANNEL_URI = BASE_URI + "sp?keep";
    private static final String OUTGOING_MATCHMAKING_CHANNEL_URI = BASE_URI + "ps?keep";

    private static String getPlayerIncomingChannelUri(String playerId) {
        return BASE_URI + "s" + playerId + "?keep";
    }

    private static String getPlayerOutgoingChannelUri(String playerId) {
        return BASE_URI + playerId + "s" + "?keep";
    }

    private final String playerId = UUID.randomUUID().toString();

    private InteractionWrapper matchmakingInteractionHandler;
    private InteractionWrapper directInteractionHandler;

    @Getter
    private ClientLobbyHandler clientLobbyHandler;

    @Getter
    private ClientFightHandler clientFightHandler;

    public void connect() {
        try {
            matchmakingInteractionHandler = new InteractionWrapper(
                    new IncomingChannel(new RemoteSpace(INCOMING_MATCHMAKING_CHANNEL_URI)),
                    new OutgoingChannel(new RemoteSpace(OUTGOING_MATCHMAKING_CHANNEL_URI))
            );

            directInteractionHandler = new InteractionWrapper(
                    new IncomingChannel(new RemoteSpace(getPlayerIncomingChannelUri(playerId))),
                    new OutgoingChannel(new RemoteSpace(getPlayerOutgoingChannelUri(playerId)))
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        // TODO: Handling disconnection gracefully is not straightforward. In order for the client to understand that
        //  the opponent has disconnected, the client must be listening to disconnection messages at all times. I've
        //  tried it once with a thread in the background that listens for the DISCONNECT command. Having a thread hang
        //  like that seems to lead to issues, but it's worth trying again.
    }

    public void startMatchmaking() {
        if (matchmakingInteractionHandler == null || directInteractionHandler == null) {
            throw new IllegalStateException("Must be connected in order to start matchmaking");
        }

        clientLobbyHandler = new ClientLobbyHandler(playerId, matchmakingInteractionHandler, directInteractionHandler);
        clientLobbyHandler.start();
    }

    public void transitionToFight(Runnable fightStartedAction, Runnable fightEndAction) {
        clientFightHandler = new ClientFightHandler(directInteractionHandler, clientLobbyHandler.getEnv());
        clientFightHandler.start(fightStartedAction, fightEndAction);
    }

    public enum PlayerNumber {
        ONE,
        TWO;

        public static PlayerNumber fromInt(int i) {
            switch (i) {
                case 1:
                    return ONE;
                case 2:
                    return TWO;
                default:
                    throw new IllegalStateException();
            }
        }
    }
}
