package com.mad.fight.fight;

import com.mad.fight.*;
import com.mad.fight.components.Anchor;
import com.mad.fight.components.ImageAsset;
import com.mad.fight.components.JavaFXEventDelegator;
import com.mad.fight.components.Position;
import com.mad.fight.net.ClientFightHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class FightScreen implements ScreenView {
    private final ScreenViewSelector screenViewSelector;
    private final ScreenViewFactory screenViewFactory;

    private final List<Drawable> drawables = new ArrayList<>();
    private final List<Updatable> updatables = new ArrayList<>();
    private final KeyHandler keyHandler = new KeyHandler();
    private final Viewport viewport = new Viewport();
    private final Player player;
    private final Shadow playerShadow;
    private final Opponent opponent;
    private final Shadow opponentShadow;
    private final Background background = new Background(viewport);
    private final Foreground foreground = new Foreground(viewport);
    private final HealthBars healthBars;
    private final Timer timer;

    public FightScreen(GameLifecycleManager gameLifecycleManager, ScreenViewSelector screenViewSelector, ScreenViewFactory screenViewFactory) {
        this.screenViewFactory = screenViewFactory;
        this.screenViewSelector = screenViewSelector;

        updatables.add(new Updatable() {
            private final Image PLAYER_ONE_WIN = new Image("png/map/player_1_wins.png");
            private final Image PLAYER_TWO_WIN = new Image("png/map/player_2_wins.png");
            private final Image TIME_UP = new Image("png/map/time_up.png");

            @Override
            public void update(long updateCount) {
                ClientFightHandler.FightContext fightContext = gameLifecycleManager.getClientFightHandler().getFightContext();

                if (fightContext.getMatchWinner() > -1) {
                    updatables.remove(player);
                    updatables.remove(opponent);

                    switch (fightContext.getMatchWinner()) {
                        case 0:
                            drawables.add(new ImageAsset(TIME_UP, new Position(Anchor.TOP_CENTER, true, false, 0, 40)));
                            break;
                        case 1:
                            drawables.add(new ImageAsset(PLAYER_ONE_WIN, new Position(Anchor.TOP_CENTER, true, false, 0, 40)));
                            break;
                        case 2:
                            drawables.add(new ImageAsset(PLAYER_TWO_WIN, new Position(Anchor.TOP_CENTER, true, false, 0, 40)));
                            break;

                    }
                }
            }
        });

        timer = new Timer(gameLifecycleManager);

        healthBars = new HealthBars(gameLifecycleManager);

        player = new Player(keyHandler, viewport, gameLifecycleManager);
        opponent = new Opponent(viewport, gameLifecycleManager);

        player.getMovementHandler().setOpponent(opponent.getMovementHandler());
        opponent.getMovementHandler().setOpponent(player.getMovementHandler());

        playerShadow = new Shadow(player.getMovementHandler(), viewport);
        opponentShadow = new Shadow(opponent.getMovementHandler(), viewport);

        viewport.setPlayer(player);
        viewport.setOpponent(opponent);

        gameLifecycleManager.transitionToFight(() -> {
            ClientFightHandler.FightContext fightContext = gameLifecycleManager.getClientFightHandler().getFightContext();
            player.getMovementHandler().setPos(new Point(fightContext.getPlayerX(), fightContext.getPlayerY()));
            opponent.getMovementHandler().setPos(new Point(fightContext.getOpponentX(), fightContext.getOpponentY()));
            player.setTransmitPosition(true);
            MusicManager.INSTANCE.startMusic(MusicManager.FIGHT_THEME);
            SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.vFight);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            keyHandler.setAcceptInput(true);
        }, () -> {
            ClientFightHandler.FightContext fightContext = gameLifecycleManager.getClientFightHandler().getFightContext();
            MusicManager.INSTANCE.stopMusic();

            switch (fightContext.getMatchWinner()) {
                case 0:
                    SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.vTimeUp);
                    break;
                case 1:
                    SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.vPlayer1Wins);
                    break;
                case 2:
                    SoundEffectsManager.INSTANCE.playSoundEffect(SoundEffectsManager.vPlayer2Wins);
                    break;
            }

            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            screenViewSelector.setCurrentScreen(screenViewFactory.createMainMenuView());
        });

        drawables.add(background);
        drawables.add(playerShadow);
        drawables.add(opponentShadow);
        drawables.add(opponent);
        updatables.add(opponent);
        drawables.add(player);
        updatables.add(player);
        drawables.add(foreground);
        updatables.add(viewport);
        drawables.add(healthBars);
        drawables.add(timer);
        updatables.add(timer);
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void setHandlers(Canvas canvas) {
        JavaFXEventDelegator<KeyEvent> keyPressedEventDelegator = new JavaFXEventDelegator<>();
        JavaFXEventDelegator<KeyEvent> keyReleasedEventDelegator = new JavaFXEventDelegator<>();

        keyHandler.setHooks(keyPressedEventDelegator, keyReleasedEventDelegator);
        keyReleasedEventDelegator.addHandler(event -> {
            if (event.getCode().equals(KeyCode.ESCAPE)) {
                screenViewSelector.setOverlayScreen(screenViewFactory.createPauseView());
            }
        });

        canvas.setOnKeyPressed(keyPressedEventDelegator::handle);
        canvas.setOnKeyReleased(keyReleasedEventDelegator::handle);
    }

    @Override
    public void clearHandlers(Canvas canvas) {
    }

    @Override
    public List<Drawable> getDrawables() {
        return drawables;
    }

    @Override
    public List<Updatable> getUpdatables() {
        return updatables;
    }
}
