package com.mad.fight.fight;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import com.mad.fight.PixelArtRenderer;
import com.mad.fight.Updatable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.*;

public class Foreground implements Drawable, Updatable {
    private static final Image foreground1 = new Image("png/map/foreground1.png");
    private static final Image foreground2 = new Image("png/map/foreground2.png");


    private final Viewport viewport;

    Foreground(Viewport viewport) {
        this.viewport = viewport;
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        Point viewPos = viewport.offset(new Point(-250 / 2, 0));
        viewPos.x *= 2;
        PixelArtRenderer.render(foreground1, viewPos, dc, gc);

        viewPos = viewport.offset(new Point(-250 / 2, 79));
        viewPos.x *= 2;
        PixelArtRenderer.render(foreground2, viewPos, dc, gc);
    }

    @Override
    public void update(long updateCount) {

    }
}
