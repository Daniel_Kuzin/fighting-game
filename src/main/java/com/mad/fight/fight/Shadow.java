package com.mad.fight.fight;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import com.mad.fight.PixelArtRenderer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.*;

public class Shadow implements Drawable {
    private static final Image shadow = new Image("png/character/shadow1.png");

    private final PlayerMovementHandler movementHandler;
    private final Viewport viewport;

    public Shadow(PlayerMovementHandler movementHandler, Viewport viewport) {
        this.movementHandler = movementHandler;
        this.viewport = viewport;
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        Point pos = new Point(movementHandler.getPos().x + 1, 94);
        Point offset = viewport.offset(pos);
        PixelArtRenderer.render(shadow, offset, !movementHandler.isDirectionRight(), new PixelArtRenderer.Crop(), dc, gc);
    }
}
