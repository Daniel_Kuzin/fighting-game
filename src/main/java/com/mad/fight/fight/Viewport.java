package com.mad.fight.fight;

import com.mad.fight.Updatable;
import lombok.Setter;

import java.awt.*;

public class Viewport implements Updatable {
    @Setter
    private Player player;

    @Setter
    private Opponent opponent;

    private Point pos = new Point(0, 0);

    public Point offset(Point objPos) {
        return new Point(objPos.x + pos.x, objPos.y + pos.y);
    }

    @Override
    public void update(long updateCount) {
        if (player != null && opponent != null) {
            int xPosDiff = Math.abs(player.getMovementHandler().getPos().x - opponent.getMovementHandler().getPos().x);
            int newXPos = -(Math.min(player.getMovementHandler().getPos().x, opponent.getMovementHandler().getPos().x) + xPosDiff / 2) + 100;

            if(newXPos < -120) {
                pos.x = -120;
            } else if(newXPos > 120) {
                pos.x = 120;
            } else {
                pos.x = newXPos;
            }
        }
    }
}