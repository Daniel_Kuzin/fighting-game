package com.mad.fight.fight;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import com.mad.fight.PixelArtRenderer;
import com.mad.fight.Updatable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.*;

public class Background implements Drawable, Updatable {
    private static final Image mainground = new Image("png/map/mainground.png");
    private static final Image background1 = new Image("png/map/background1.png");
    private static final Image background2 = new Image("png/map/background2.png");
    private static final Image background3 = new Image("png/map/background3.png");
    private static final Image background4 = new Image("png/map/background4.png");
    private static final Image background5 = new Image("png/map/background5.png");


    private final Viewport viewport;

    Background(Viewport viewport) {
        this.viewport = viewport;
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext dc) {
        Point viewPos = viewport.offset(new Point(-400 / 2, 0));
        viewPos.x *= 0.5;
        PixelArtRenderer.render(background5, viewPos, dc, gc);

        viewPos = viewport.offset(new Point(-400 / 2, 0));
        viewPos.x *= 0.6;
        PixelArtRenderer.render(background4, viewPos, dc, gc);

        viewPos = viewport.offset(new Point(-400 / 2, 0));
        viewPos.x *= 0.7;
        PixelArtRenderer.render(background3, viewPos, dc, gc);

        viewPos = viewport.offset(new Point(-400 / 2, 0));
        viewPos.x *= 0.8;
        PixelArtRenderer.render(background2, viewPos, dc, gc);

        viewPos = viewport.offset(new Point(-346 / 2, 45));
        viewPos.x *= 0.9;
        PixelArtRenderer.render(background1, viewPos, dc, gc);

        viewPos = viewport.offset(new Point(-240 / 2, 42));
        PixelArtRenderer.render(mainground, viewPos, dc, gc);
    }

    @Override
    public void update(long updateCount) {

    }
}
