package com.mad.fight.fight;

import com.mad.fight.Drawable;
import com.mad.fight.DrawingContext;
import com.mad.fight.GameLifecycleManager;
import com.mad.fight.PixelArtRenderer;
import com.mad.fight.net.ClientFightHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.*;

public class HealthBars implements Drawable {
    private static final Image healthbar = new Image("png/map/healthbar.png");
    private static final Image healthtick = new Image("png/map/healthtick.png");

    private final GameLifecycleManager gameLifecycleManager;

    public HealthBars(GameLifecycleManager gameLifecycleManager) {
        this.gameLifecycleManager = gameLifecycleManager;
    }

    @Override
    public void draw(GraphicsContext gc, long updateCount, DrawingContext context) {
        ClientFightHandler.FightContext fightContext = gameLifecycleManager.getClientFightHandler().getFightContext();

        PixelArtRenderer.render(healthbar, new Point(7, 5), context, gc);

        if (fightContext.getPlayerNumber().equals(GameLifecycleManager.PlayerNumber.ONE)) {
            PixelArtRenderer.render(healthtick, new Point(9, 7), false, new PixelArtRenderer.Crop(100 - fightContext.getPlayerHealth(), 0, 0, 0), context, gc);
            PixelArtRenderer.render(healthtick, new Point(130, 7), false, new PixelArtRenderer.Crop(0, 0, 100 - fightContext.getOpponentHealth(), 0), context, gc);
        } else {
            PixelArtRenderer.render(healthtick, new Point(130, 7), false, new PixelArtRenderer.Crop(0, 0, 100 - fightContext.getPlayerHealth(), 0), context, gc);
            PixelArtRenderer.render(healthtick, new Point(9, 7), false, new PixelArtRenderer.Crop(100 - fightContext.getOpponentHealth(), 0, 0, 0), context, gc);
        }
    }
}
