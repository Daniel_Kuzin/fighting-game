package com.mad.fight.net;

import lombok.Getter;
import org.jspace.FormalField;
import org.jspace.TemplateField;

import java.util.Arrays;
import java.util.Optional;

import static com.mad.fight.net.InteractionWrapper.af;
import static com.mad.fight.net.InteractionWrapper.ff;

public enum Command {
    CONNECTION_REQUEST(1, ff(Integer.class)),
    DISCONNECT(2),
    LOBBY_REDIRECT(3, ff(Integer.class), ff(String.class)),
    MATCH_BRIEF(4, ff(Integer.class)),
    SELECT_CHARACTER(5, ff(Integer.class)),
    OPPONENT_CHARACTER_SELECTED(6, ff(Integer.class)),
    READY(7),
    UNREADY(8),
    OPPONENT_READY(9),
    OPPONENT_UNREADY(10),
    BOTH_PLAYERS_READY(11),
    STOP_LOBBY_UPDATES(12),
    PREPARE_FOR_MATCH_START(13),
    READY_FOR_MATCH_START(14),
    MATCH_START(15),
    INPUT_CODE_UPDATE(16, false, ff(Integer.class), ff(Boolean.class), ff(Boolean.class)),
    MINIMAL_STATE_UPDATE(17, false, ff(Integer.class), ff(Integer.class), ff(Boolean.class)),
    CONTINUE_FULL_STATE_UPDATE(18, false, ff(Boolean.class)),
    TIME_UPDATE(19, false, ff(Integer.class)),
    POSITION_UPDATE(20, false, ff(Integer.class), ff(Integer.class), ff(Integer.class), ff(Integer.class)),
    OPPONENT_POSITION_UPDATE(21, false, ff(Integer.class), ff(Integer.class), ff(Integer.class), ff(Integer.class)),
    GAME_ACTIVE(22, false, ff(Boolean.class)),
    MATCH_WINNER(23, false, ff(Integer.class)),
    OPPONENT_HEALTH_UPDATE(24, false, ff(Integer.class));

    @Getter
    private final int code;
    @Getter
    private final boolean requireBriefing;
    private final FormalField[] formalFields;

    Command(int code, FormalField... formalFields) {
        this.code = code;
        this.requireBriefing = true;
        this.formalFields = formalFields;
    }

    Command(int code, boolean requireBriefing, FormalField... formalFields) {
        this.code = code;
        this.requireBriefing = requireBriefing;
        this.formalFields = formalFields;

        if(this.formalFields.length == 0) {
            throw new IllegalStateException();
        }
    }

    public TemplateField[] getSignature() {
        TemplateField[] arr = new TemplateField[formalFields.length + 1];
        arr[0] = af(code);
        System.arraycopy(formalFields, 0, arr, 1, formalFields.length);
        return arr;
    }

    public static Optional<Command> fromCode(int code) {
        return Arrays.stream(Command.values()).filter(c -> c.getCode() == code).findFirst();
    }

    // Tests for duplicate codes
    public static void main(String[] args) {
        int length = Command.values().length;
        int distinctLength = (int) Arrays.stream(Command.values()).map(Command::getCode).distinct().count();

        if (distinctLength != length) {
            throw new IllegalStateException("There are duplicate codes");
        }
    }
}
