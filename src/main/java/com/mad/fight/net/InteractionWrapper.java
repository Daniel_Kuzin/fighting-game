package com.mad.fight.net;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jspace.*;

@RequiredArgsConstructor
public class InteractionWrapper {
    @Getter
    private final IncomingChannel incoming;
    private final OutgoingChannel outgoing;

    public void send(Command command, Object... fields) throws InterruptedException {
        outgoing.send(command, fields);
    }

    public Object[] await(Command command, TemplateField... fields) throws InterruptedException {
        return incoming.await(command, fields);
    }

    IncomingChannel.ReceivedCommand awaitAnyCommand() throws InterruptedException {
        return incoming.awaitAnyCommand();
    }

    static void removeSpaces(String incomingChannelName, String outgoingChannelName, SpaceRepository spaceRepository) {
        spaceRepository.remove(incomingChannelName);
        spaceRepository.remove(outgoingChannelName);
    }

    static InteractionWrapper createAndAddSpaces(String incomingChannelName, String outgoingChannelName, SpaceRepository spaceRepository) {
        Space incomingSpace = new SequentialSpace();
        Space outgoingSpace = new SequentialSpace();
        InteractionWrapper interactionWrapper = new InteractionWrapper(new IncomingChannel(incomingSpace), new OutgoingChannel(outgoingSpace));
        spaceRepository.add(incomingChannelName, incomingSpace);
        spaceRepository.add(outgoingChannelName, outgoingSpace);
        return interactionWrapper;
    }

    public static FormalField ff(Class<?> c) {
        return new FormalField(c);
    }

    public static ActualField af(Object f) {
        return new ActualField(f);
    }

    public static <T> T getSingleField(Object[] arr) {
        //noinspection unchecked
        return (T) arr[1];
    }
}
