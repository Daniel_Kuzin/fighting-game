package com.mad.fight;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MusicManager {
    public static final Media MAIN_THEME = new Media(MusicManager.class.getResource("/music/fighter.wav").toString());
    public static final Media CHARACTER_SELECT = new Media(MusicManager.class.getResource("/music/character_select.wav").toString());
    public static final Media FIGHT_THEME = new Media(MusicManager.class.getResource("/music/fight_theme.wav").toString());
    public static final MusicManager INSTANCE = new MusicManager();

    private MediaPlayer mediaPlayer;

    private MusicManager() {
        mediaPlayer = new MediaPlayer(MAIN_THEME);
        mediaPlayer.setVolume(SettingsManager.INSTANCE.getSettings().getMusicVolume());
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        mediaPlayer.play();

        SettingsManager.INSTANCE.addChangedHandler(gameSettings -> mediaPlayer.setVolume(gameSettings.getMusicVolume()));
    }

    public void stopMusic() {
        mediaPlayer.stop();
    }

    public void startMusic(Media track) {
        if (track.equals(mediaPlayer.getMedia())) {
            return;
        }

        mediaPlayer = new MediaPlayer(track);
        mediaPlayer.setVolume(SettingsManager.INSTANCE.getSettings().getMusicVolume());
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        mediaPlayer.play();
    }
}
