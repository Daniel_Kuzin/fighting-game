package com.mad.fight;

import lombok.Data;

@Data
public class DrawingContext {
    private final int width;
    private final int height;
    private final int scalingFactor;

    public int getScreenWidth() {
        return width * scalingFactor;
    }

    public int getScreenHeight() {
        return height * scalingFactor;
    }
}
