package com.mad.fight;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Files;
import java.nio.file.Path;

@Log4j2
public class SettingsManager extends FileBasedStorageHandler<GameSettings> {
    public static final SettingsManager INSTANCE = new SettingsManager();

    private GameSettings cache = loadSettings();

    private SettingsManager() {
    }

    public void saveSettings(GameSettings gameSettings) {
        cache = gameSettings;

        log.info("Saved settings: " + gameSettings);
        DrawingContextProvider.initialize(gameSettings.getScalingFactor());
        save(gameSettings, getAppDataPath().resolve("settings.ser"));
    }

    public GameSettings getSettings() {
        return cache;
    }

    private GameSettings loadSettings() {
        Path settingsFile = getAppDataPath().resolve("settings.ser");

        if (!Files.exists(settingsFile)) {
            return new GameSettings();
        }

        GameSettings gameSettings = load(settingsFile);
        log.info("Loaded settings: " + gameSettings);

        if(Program.DEBUG_OPTIONS.contains("muted")) {
            gameSettings.setMusicVolume(0);
            gameSettings.setSfxVolume(0);
        }

        cache = gameSettings;
        return gameSettings;
    }
}
